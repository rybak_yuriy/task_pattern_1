package com.group6.controller;

import com.group6.factory.Bakery;
import com.group6.factory.City;
import com.group6.factory.DniproBakery;
import com.group6.factory.LvivBakery;
import com.group6.model.pizza.Pizza;
import com.group6.model.pizza.PizzaType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Controller {
    private static Logger log = LogManager.getLogger(Controller.class);

    Bakery bakery;
    List<Pizza> pizzaList;
    Scanner scanner = new Scanner(System.in);

    public Controller() {
        pizzaList = new ArrayList<>();
    }

    public List<Pizza> makeOrder() {
        log.info("Choose your city:" + '\n' +
                "1 - Lviv" + '\n' +
                "2 - Dnipro" + '\n' +
                "3 - Kyiv ");
        int c = scanner.nextInt();
        City city = getCity(c);
        bakery = getBakery(city);
        log.info("Enter quantity of pizzas:");
        int quantity = scanner.nextInt();
        for (int i = 1; i <= quantity; i++) {
            log.info("Choose type of " + i + " pizza:" + '\n' +
                    "1 - Cheese" + '\n' +
                    "2 - Pepperoni" + '\n' +
                    "3 - Clam" + '\n' +
                    "4 - Veggie");
            int type = scanner.nextInt();
            PizzaType pizzaType = getType(type);
            Pizza pizza = bakery.bake(pizzaType);
            pizzaList.add(pizza);
        }
        return pizzaList;
    }

    private City getCity(int i) {
        City city = null;
        if (i == 1) {
            city = City.LVIV;
        }
        if (i == 2) {
            city = City.DNIPRO;
        }
        if (i == 3) {
            city = City.KYIV;
        }
        return city;
    }

    private Bakery getBakery(City city) {
        if (city == City.LVIV) {
            return new LvivBakery();
        }
        if (city == City.DNIPRO) {
            return new DniproBakery();
        }
        if (city == City.KYIV) {
            return new LvivBakery();
        }
        return bakery;
    }

    private PizzaType getType(int i) {
        PizzaType pizzaType = null;

        if (i == 1) {
            pizzaType = PizzaType.CHEESE;
        }
        if (i == 2) {
            pizzaType = PizzaType.PEPPERONI;
        }
        if (i == 3) {
            pizzaType = PizzaType.CLAM;
        }
        if (i == 4) {
            pizzaType = PizzaType.VEGGIE;
        }
        return pizzaType;
    }

}
