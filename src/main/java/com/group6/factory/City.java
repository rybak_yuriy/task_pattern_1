package com.group6.factory;

public enum City {

    LVIV,
    DNIPRO,
    KYIV
}
