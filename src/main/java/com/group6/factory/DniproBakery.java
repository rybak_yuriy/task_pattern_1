package com.group6.factory;

import com.group6.model.components.Topping;
import com.group6.model.pizza.*;

public class DniproBakery implements Bakery {

    @Override
    public Pizza bake(PizzaType type) {
        Pizza pizza = null;
        if (type == PizzaType.CHEESE) {
            pizza = new CheesePizza();
            pizza.getToppings().add(Topping.OLIVES);
        }
        if (type == PizzaType.CLAM) {
            pizza = new ClamPizza();
            pizza.getToppings().add(Topping.CHICKEN);
        }
        if (type == PizzaType.PEPPERONI) {
            pizza = new PepperoniPizza();
            pizza.getToppings().add(Topping.PINEAPPLE);
        }
        if (type == PizzaType.VEGGIE) {
            pizza = new VeggiePizza();
            pizza.getToppings().add(Topping.PINEAPPLE);
        }
        return pizza;
    }

}
