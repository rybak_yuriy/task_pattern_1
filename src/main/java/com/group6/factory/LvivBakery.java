package com.group6.factory;

import com.group6.model.components.Topping;
import com.group6.model.pizza.*;

import java.util.ArrayList;
import java.util.List;

public class LvivBakery implements Bakery {

    List<Pizza> pizzaList;

    public LvivBakery() {
        pizzaList = new ArrayList<>();
    }

    @Override
    public Pizza bake(PizzaType type) {
        Pizza pizza = null;
        if (type == PizzaType.CHEESE) {
            pizza = new CheesePizza();
            pizza.getToppings().add(Topping.MUSHROOMS);
        }
        if (type == PizzaType.CLAM) {
            pizza = new ClamPizza();
            pizza.getToppings().add(Topping.RED_PEPPER);
        }
        if (type == PizzaType.PEPPERONI) {
            pizza = new PepperoniPizza();
            pizza.getToppings().add(Topping.RED_PEPPER);
        }
        if (type == PizzaType.VEGGIE) {
            pizza = new VeggiePizza();
            pizza.getToppings().add(Topping.RED_PEPPER);
        }
        return pizza;
    }

}


