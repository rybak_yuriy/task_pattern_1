package com.group6.factory;

import com.group6.model.pizza.Pizza;
import com.group6.model.pizza.PizzaType;

public interface Bakery {

    Pizza bake(PizzaType type);
}
