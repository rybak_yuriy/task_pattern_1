package com.group6.view;

import com.group6.controller.Controller;
import com.group6.model.pizza.Pizza;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class MyView {

    private static Logger log = LogManager.getLogger(Controller.class);
    Scanner scanner = new Scanner(System.in);
    Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> menuMethods;

    public MyView() {
        controller = new Controller();
        menu = new LinkedHashMap<>();
        menuMethods = new LinkedHashMap<>();

        menu.put("1", "1 - Start your order: ");
        menu.put("Q", "Q - Exit");

        menuMethods.put("1", this::makeOrder);
    }

    private void makeOrder() {
        List<Pizza> pizzaList = controller.makeOrder();
        log.info("Your order is:");
        pizzaList.forEach((System.out::println));
        log.info("Thank You for your order!");
    }


    private void outputMenu() {
        System.out.println("\nMENU:");
        menu.values().forEach(System.out::println);
    }

    public void start() {
        String keyMenu;
        do {
            outputMenu();
            System.out.print("Please, select menu point: ");
            keyMenu = scanner.nextLine().toUpperCase();
            try {
                menuMethods.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
        scanner.close();
        System.exit(0);
    }


}
