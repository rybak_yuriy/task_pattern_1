package com.group6.model.components;

public enum  Sauce {

    MARINARA,
    PESTO,
    TOMATO,
    SALSA,
    CHILI,
    SWEET_PEPPER

}
