package com.group6.model.components;

public enum  Topping {

    CHEESE,
    SALAMI,
    CHICKEN,
    PEPPERONI,
    OLIVES,
    MUSHROOMS,
    ONION,
    PINEAPPLE,
    BASIL,
    HAM,
    CLAMS,
    RED_PEPPER,

}
