package com.group6.model.pizza;

import com.group6.model.components.Dough;
import com.group6.model.components.Sauce;
import com.group6.model.components.Topping;

import java.util.HashSet;
import java.util.Set;

public class PepperoniPizza extends Pizza {

    public PepperoniPizza() {
        setName("Pepperoni");
        setDough(Dough.THIN);
        setSauce(Sauce.PESTO);
        setToppings(chooseToppings());
    }

    @Override
    protected Set<Topping> chooseToppings() {
        HashSet<Topping> toppings = new HashSet<>();
        toppings.add(Topping.PEPPERONI);
        toppings.add(Topping.BASIL);
        toppings.add(Topping.SALAMI);
        toppings.add(Topping.ONION);
        toppings.add(Topping.OLIVES);
        return toppings;
    }
}
