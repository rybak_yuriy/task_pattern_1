package com.group6.model.pizza;

import com.group6.model.components.Dough;
import com.group6.model.components.Sauce;
import com.group6.model.components.Topping;

import java.util.HashSet;
import java.util.Set;

public class ClamPizza extends Pizza {

    public ClamPizza() {
        setName("Clam");
        setDough(Dough.THIN);
        setSauce(Sauce.TOMATO);
        setToppings(chooseToppings());
    }

    @Override
    protected Set<Topping> chooseToppings() {
        HashSet<Topping> toppings = new HashSet<>();
        toppings.add(Topping.CLAMS);
        toppings.add(Topping.BASIL);
        toppings.add(Topping.OLIVES);
        toppings.add(Topping.ONION);
        return toppings;
    }
}
