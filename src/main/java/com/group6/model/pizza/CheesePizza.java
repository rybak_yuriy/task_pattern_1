package com.group6.model.pizza;

import com.group6.model.components.Dough;
import com.group6.model.components.Sauce;
import com.group6.model.components.Topping;

import java.util.HashSet;
import java.util.Set;

public class CheesePizza extends Pizza {

    public CheesePizza() {
        setName("Cheese");
        setDough(Dough.THIN);
        setSauce(Sauce.CHILI);
        setToppings(chooseToppings());
    }

    @Override
    protected Set<Topping> chooseToppings() {
        HashSet<Topping> toppings = new HashSet<>();
        toppings.add(Topping.CHEESE);
        toppings.add(Topping.PINEAPPLE);
        toppings.add(Topping.BASIL);
        toppings.add(Topping.MUSHROOMS);
        toppings.add(Topping.HAM);
        return toppings;
    }


}
