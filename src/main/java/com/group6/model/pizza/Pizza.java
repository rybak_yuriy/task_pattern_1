package com.group6.model.pizza;

import com.group6.model.components.Dough;
import com.group6.model.components.Sauce;
import com.group6.model.components.Topping;

import java.util.Set;

public abstract class Pizza {

    String name;
    Dough dough;
    Sauce sauce;
    Set<Topping> toppings;

    public Pizza() {

    }

    public void setName(String name) {
        this.name = name;
    }

    protected abstract Set<Topping> chooseToppings();

    public void setDough(Dough dough) {
        this.dough = dough;
    }

    public void setSauce(Sauce sauce) {
        this.sauce = sauce;
    }

    public Set<Topping> getToppings() {
        return toppings;
    }

    public void setToppings(Set<Topping> toppings) {
        this.toppings = toppings;
    }

    @Override
    public String toString() {
        return "Pizza{" +
                "name='" + name + '\'' +
                ", dough=" + dough +
                ", sauce=" + sauce +
                ", toppings=" + toppings +
                '}';
    }
}
